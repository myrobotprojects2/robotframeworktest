*** Settings ***
Library     SapGuiLibrary
Library     DateTime

*** Variables ***
${SAPLOGONSCREEN}       /app/con[0]/ses[0]/wnd[0]
${URL_GRID}             /usr/subSUB0:SAPLMEGUI:0013/subSUB1:SAPLMEVIEWS:1100/subSUB2:SAPLMEVIEWS:1200/subSUB1:SAPLMEGUI:1102/tabsHEADER_DETAIL/tabpTABHDT9/ssubTABSTRIPCONTROL2SUB:SAPLMEGUI:1221
${ITENS_PEDIDO}         /usr/subSUB0:SAPLMEGUI:0013/subSUB2:SAPLMEVIEWS:1100/subSUB2:SAPLMEVIEWS:1200/subSUB1:SAPLMEGUI:1211/tblSAPLMEGUITC_1211
${GRID_NCM}             /usr/subSUB0:SAPLMEGUI:0010/subSUB3:SAPLMEVIEWS:1100/subSUB2:SAPLMEVIEWS:1200/subSUB1:SAPLMEGUI:1301/subSUB2:SAPLMEGUI:1303/tabsITEM_DETAIL/tabpTABIDT11/ssubTABSTRIPCONTROL1SUB:SAPLMEGUI:1326
${SESSION_FRAME}        /app/con[0]/ses[0]/wnd[1]

*** Keywords ***
Preencher transacao "${transacao}"
    Run Transaction     ${transacao}
    SetFocus            ${SAPLOGONSCREEN}/tbar[0]/okcd
    Send Vkey      0

Digitar fornecedor "${fornecedor}"
    Sleep    2
    Input Text  ${SAPLOGONSCREEN}/usr/subSUB0:SAPLMEGUI:0013/subSUB0:SAPLMEGUI:0030/subSUB1:SAPLMEGUI:1105/ctxtMEPO_TOPLINE-SUPERFIELD  ${fornecedor}
    Send Vkey    0

Preencher Org.compras "${orgcompras}"
    Sleep    4
    Input Text    ${SAPLOGONSCREEN}${URL_GRID}/ctxtMEPO1222-EKORG    ${orgcompras}
    Send Vkey    0
Preencher Grp.compras "${grpcompras}"
    Input Text    ${SAPLOGONSCREEN}${URL_GRID}/ctxtMEPO1222-EKGRP   ${grpcompras}
    Send Vkey    0

Preencher item "${item}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/txtMEPO1211-EBELP[1,0]     ${item}

Preencher coluna c "${colunac}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-KNTTP[2,0]    ${colunac}

Prencher material "${material}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-EMATN[4,0]    ${material}

Preencher texto breve "${textobreve}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/txtMEPO1211-TXZ01[5,0]     ${textobreve}

Quantidade pedido "${qtdpedido}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/txtMEPO1211-MENGE[6,0]     ${qtdpedido}

Informar campo "${pec}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-MEINS[7,0]      ${pec}

Informar data remessa "${dataremessa}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-EEIND[9,0]      ${dataremessa}
    Set Focus    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-EEIND[9,0]

Preencher campo preço "${valor}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/txtMEPO1211-NETPR[10,0]    ${valor}

Preencher campo por "${campopor}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/txtMEPO1211-PEINH[12,0]   ${campopor}

Preencger campo upp "${upp}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-BPRME[13,0]   ${upp}

Prencher campo centro "${centro}"
    Input Text    ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-NAME1[15,0]   ${centro}
    Set Focus   ${SAPLOGONSCREEN}${ITENS_PEDIDO}/ctxtMEPO1211-NAME1[15,0]
    Send Vkey    0

    Set Focus    ${SESSION_FRAME}/usr/lbl[1,3]
    Send Vkey    2
#
Preencher CodNCM "${ncm}"
    Input Text    ${SAPLOGONSCREEN}${GRID_NCM}/ctxtMEPO1326-J_1BNBM   ${ncm}
    Send Vkey    0

Clicar no botão gravar
    Sleep    2
    Send Vkey    11

Conferir mensagem de sucesso "${mensagem}"
    ${value_mensagem} =  Get Value    ${SAPLOGONSCREEN}/sbar
    Should Contain    ${value_mensagem}  ${mensagem}
    Take Screenshot

Close application SAPLogon
    Send Vkey    12
    Send Vkey    15
    Click Element    ${SESSION_FRAME}/usr/btnSPOP-OPTION1
