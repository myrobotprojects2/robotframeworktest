*** Settings ***
Library         SapGuiLibrary

*** Variables ***
${AMBIENTE}          Ambiente: LA1 (250)
${SESSAOSAP}        /app/con[0]/ses[0]/wnd[0]

*** Keywords ***
Conectar na sessão SAPLogon
    Connect To Session
    Open Connection    ${AMBIENTE}

Preencher dados login "${username}" e senha "${pass}"
    Input Text    ${SESSAOSAP}/usr/txtRSYST-BNAME   ${username}
    Input Text    ${SESSAOSAP}/usr/pwdRSYST-BCODE   ${pass}
    Set Focus     ${SESSAOSAP}/usr/pwdRSYST-BCODE
    Send Vkey   0
