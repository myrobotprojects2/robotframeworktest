*** Settings ***
Library         SapGuiLibrary
Suite Setup     Iniciar aplicação SAPLogon
Suite Teardown  Fechar aplicação SAPLogon
Resource        ../resources/LoginPage.robot
Resource        ../resources/HomePage.robot

*** Variables ***
${SESSION_FRAME}    /app/con[0]/ses[0]/wnd[1]

*** Keywords ***
Iniciar aplicação SAPLogon
    Sleep    1

Fechar aplicação SAPLogon
    # Send Vkey    15
    # Click Element    ${SESSION_FRAME}/usr/btnSPOP-OPTION1
    Take Screenshot

*** Test Case ***
Cenário 01: Efetuar Pedido SAPGui
    Iniciar aplicação SAPLogon
    Conectar na sessão SAPLogon
    Preencher dados login "agiffhorn" e senha "agiffhorn"
    Preencher transacao "ME21N"
    Digitar fornecedor "380003"
    Preencher Org.compras "1000"
    Preencher Grp.compras "100"
    Preencher item "1"
    Preencher coluna c "K"
    Prencher material "6"
    Preencher texto breve "Vestuario"
    Quantidade pedido "1"
    Informar campo "PEC"
    Informar data remessa "04.03.2020"
    Preencher campo preço "109,97"
    Preencher campo por "1"
    Preencger campo upp "PEC"
    Prencher campo centro "Centro 1"
    Preencher CodNCM "0101.90.90"
    Clicar no botão gravar
    Conferir mensagem de sucesso "Pedido normal criado sob o nº"
    Close application SAPLogon
